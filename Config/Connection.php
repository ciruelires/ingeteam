<?php

namespace Config;

use PDO;

class Connection
{
    public static function getConnection(){
        try {
            $hostname   = "localhost";
            $username   = "root";
            $password   = "";
            $dbname     = "IngeteamTest";

            $connection = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);

            return $connection;
        }
        catch(PDOException $e)
        {
            return null;
        }
    }
}
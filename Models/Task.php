<?php

namespace Models;

use Config\Connection as Connection;

include('DBModel.php');

class Task extends DBModel
{
    protected $name;
    protected $mail;
    protected $description;
    protected $address;
    protected $cp;
    protected $password;

    public function save(){
        include('../Config/Connection.php');
        $connection = Connection::getConnection();
        $stmt = $connection->prepare("INSERT INTO task (name, email, description, address, cp, password) 
                                      VALUES (:name, :email, :description, :address, :cp, :password)");

        $bindings = [
            'name' => $this->name,
            'email' => $this->email,
            'description' => $this->description,
            'address' => $this->address,
            'cp' => $this->cp,
            'password' => password_hash($this->password, PASSWORD_BCRYPT)//Encrypt pass
        ];
        $stmt->execute($bindings);
        $connection = null;
    }

    public static function getAll(){
        $connection = Connection::getConnection();
        return $connection->query("SELECT name, email, description, address, cp FROM task")->fetchAll();
    }
}
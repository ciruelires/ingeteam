<?php

namespace Models;

abstract class DBModel
{
    public function __construct($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
    }

    public abstract function save();
}
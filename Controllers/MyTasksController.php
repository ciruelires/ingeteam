<?php

namespace Controllers;

include('Models/Task.php');

use Models\Task as Task;

class MyTasksController
{
    public static function getTasks()
    {
        return Task::getAll();
    }

}
?>
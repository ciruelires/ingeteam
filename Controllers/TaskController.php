<?php

namespace Controllers;

include('../Models/Task.php');

use Models\Task as Task;

class TaskController
{
    public static function saveTask()
    {
        $params = ['name'   => $_POST['name'],
            'email'         => $_POST['email'],
            'description'   => $_POST['description'],
            'address'       => $_POST['address'],
            'cp'            => $_POST['cp'],
            'password'      => $_POST['password'],];

        $task = new Task($params);
        $task->save();
    }

    public static function validate($params)
    {
        $blankFields = [];
        foreach ($params as $key => $param) {
            if (!$param) {
                $blankFields[] = $key;
            }
        }
        //No blank fields
        if (!empty($blankFields)) {
            throw new \Exception('The following fields cannot be blank: ' . implode(", ", $blankFields));
        }
        //Name only has letters
        if (preg_match('~[0-9]~', $_POST['name'])) {
            throw new \Exception('Name cannot contain numbers');
        }
        //Valid email format
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Invalid email format');
        }
        //Valid CP format
        if (strlen($_POST['cp']) != 5 || preg_match('/[a-z]/i', $_POST['cp'])) {
            throw new \Exception('Invalid CP format');
        }
    }
}
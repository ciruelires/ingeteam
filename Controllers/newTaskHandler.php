<?php

include('TaskController.php');
use Controllers\TaskController as TaskController;

try {
    TaskController::validate($_POST);
    TaskController::saveTask();

    echo "ok";
}
catch(\Exception $exception) {
    echo $exception->getMessage();
}
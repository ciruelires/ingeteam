$(function(){
  $("#saveTask").click( function(){
      var params = {
        'name'          : $('#name').val(),
        'email'         : $('#email').val(),
        'description'   : $('#description').val(),
        'address'       : $('#address').val(),
        'cp'            : $('#cp').val(),
        'password'      : $('#password').val(),
      };
      $.post("Controllers/newTaskHandler.php", params, "json").done(function(data) {
        if(data == 'ok'){
          $("#messages").css('color' , 'green');
          $("#messages").text('Task registered successfully');
          //Remove input values
          $('input').each(function(field){
            $(this).val("");
          })
          $('textarea').val("");
          setTimeout(function(){
            $("#messages").text("");
          }, 1500);
        }
        else {
          $("#messages").css('color' , 'red');
          $("#messages").text(data);
        }
      })
    }
  );
});
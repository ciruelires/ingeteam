<?php
include('Controllers/MyTasksController.php');
use Controllers\MyTasksController as MyTasksController;

$view = 'MyTasks.php';

$tasks = MyTasksController::getTasks();
?>
<div class="containerList">
  <?php
  if(empty($tasks)){
  ?>
    <p class="notasks">There are no tasks to show</p>
  <?php
  }
  else {
      ?>
    <table>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>CP</th>
        <th>Description</th>
      </tr>
        <?php
        foreach ($tasks as $task) {
            ?>
          <tr>
            <td><?= $task['name'] ?></td>
            <td><?= $task['email'] ?></td>
            <td><?= $task['address'] ?></td>
            <td><?= $task['cp'] ?></td>
            <td class="description"><?= $task['description'] ?></td>
          </tr>
            <?php
        }
        ?>
    </table>
    </div>
      <?php
  }

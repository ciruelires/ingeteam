<div class="container">
  <div class="left">
    <div class="field">
      <label for="name">Name:</label>
      <input type="text" name='name' id='name'> <!--Only letters-->
    </div>
    <div class="field">
      <label for="email">Email:</label>
      <input type="email" name='email' id='email'>
    </div>
    <div class="field">
      <label for="password">Password:</label>
      <input type="password" name='password' id='password'>
    </div>
  </div>
  <div class="right">
    <div class="field">
      <label for="address">Address:</label>
      <input type="text" name='address' id='address'>
    </div>
    <div class="field">
      <label for="cp">CP:</label>
      <input type="text" name='cp' minlength="5" maxlength="5" id='cp'>
    </div>
  </div>
  <div class="description field">
    <label for="description">Description:</label>
    <textarea name='description' id='description' maxlength="500"></textarea>
  </div>
  <button id="saveTask">Send</button>
  <p id="messages"></p>
</div>